<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php // utilizando helper img

 echo Html::img("@web/imgs/{$foto}",[
    'id' => 'foto',
    'alt' => 'xeni',
    'width' => '300'
    ]); 
?>

<?php // utilizando helper Url ?>
<img src="<?= Url::to("@web/imgs/{$foto}") ?>" width="300">

<?php // utilizando el metodo getAlias?>
<img src="<?= Yii::getAlias('@web') . "/imgs/{$foto}" ?>"
     width=300">



