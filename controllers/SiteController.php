<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionMensaje() {
        return $this->render("vista1");
    }
    
    public function actionPresentar(){
        return $this->render("presentar",[
            "mensaje" => "Probando argumentos"
        ]);
    }
    public function actionDespedir(){
        return $this->render("presentar",[
            "mensaje" => "Adios"
        ]);
    }
     public function actionMensaje3(){
         $salida="<ul><strong>";
         for($c=1;$c<100;$c+=2){
        $salida.="<li>{$c}</li>";
    }
        $salida.="</strong></ul>";
        return $this->render("listado", [
            "datos" => $salida,
                ]);
    }
     public function actionMensaje4(){
         $salida="<ul><strong>";
         for($c=2;$c<101;$c+=2){
        $salida.="<li>{$c}</li>";
    }
        $salida.="</strong></ul>";
        return $this->render("listado", [
            "datos" => $salida,
                ]);
    }
    
    // esta accion recibe un parametro por get
    public function actionFotos($id){
        $fotos=[
            'xeni.jpg',
            'xeni1.jpg'
        ];
        
        return $this->render("fotos",[
            "foto" => $fotos[$id-1]
        ]);
    }
}
